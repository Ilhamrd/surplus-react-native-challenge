import React, { useEffect } from 'react';
import { NativeBaseProvider } from 'native-base';
import { NavigationContainer } from '@react-navigation/native';
import SplashScreen from 'react-native-splash-screen';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { RecoilRoot } from 'recoil';

import AuthStack from './src/navigations/AuthStack';

const App = () => {
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <SafeAreaProvider>
      <RecoilRoot override={false}>
        <NavigationContainer>
          <NativeBaseProvider>
            <AuthStack />
          </NativeBaseProvider>
        </NavigationContainer>
      </RecoilRoot>
    </SafeAreaProvider>
    
  );
};

export default App;
