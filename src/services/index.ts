import axios from "axios"
import { API } from "../utils/api"

export const getListBaner = async (size: number) => {
    try {
        const response = await axios({
            method: 'GET',
            url: API.pokemon.list(size)
        })

        return response;
    }catch (e) {
		throw e;
	}
}