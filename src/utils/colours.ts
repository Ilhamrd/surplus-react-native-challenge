const colours = {
    transparent: 'transparent',
    white: '#FFFFFF',
    black: '#000000',
    warning: '#FD9C2A',
    successGreen: '#629A4F',
    successBgGreen: '#E4F3E0',
    successMainGreen: '#229A16',
    warningBgOrange: '#FFF5E3',
    dangerBgRed: '#FF0000',
    text: '#55BBA6',
    textBold: '#424242',
    primary: '#009788'
  };
  
  export default colours;