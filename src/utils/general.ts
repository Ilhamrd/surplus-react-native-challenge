export const checkEmail = (value: string) : boolean => {
    const pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/; 
    if(pattern.test(value))
        return true;
    return false;
}