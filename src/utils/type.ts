export declare namespace SurplusType {
    export interface error {
        valid: boolean,
        message: string
    }
    export interface user {
        email: string,
        password: string
    }
}