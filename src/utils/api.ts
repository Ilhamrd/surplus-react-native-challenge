export const API_BASE_URL = 'https://pokeapi.co/api/v2';

export const API = {
    pokemon: {
        list: (size: number) =>  `${API_BASE_URL}/pokemon?limit=${size}&offset=0`
    }
}