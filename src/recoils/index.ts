import { atom } from "recoil";
import { DATA_USER, IS_LOGGIN } from "./constants";
import { SurplusType } from "../utils/type";

export const isLogin = atom<boolean>({
    key: IS_LOGGIN,
    default: false
})

export const dataUser = atom<SurplusType.user>({
    key: DATA_USER,
    default: null
})