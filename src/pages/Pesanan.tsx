import React from 'react';
import {
  Text,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
  Image,
  View,
} from 'react-native';

const Pesanan = () => {
  return (
    <SafeAreaView style={styles.container}>
      <View>
        <Text style={styles.txtTitle}>Daftar Pesanan</Text>
        <Image
          source={require('../assets/images/bg-empty.png')}
          style={styles.bgimg}
        />
      </View>
      <View style={styles.viewPesanan}>
        <Text style={styles.txtPesan}>Kamu belum punya pesanan nih</Text>
        <Text style={styles.txtIsian}>
          Segera pesan dan selamatkan makanan favoritmu
        </Text>
        <TouchableOpacity style={styles.btnPesan}>
          <Text style={styles.txtPsn}>Pesan Sekarang</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  txtTitle: {
    color: '#000',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 10,
  },
  bgimg: {
    width: 200,
    height: 200,
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 30,
  },
  viewPesanan: {
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
  txtPesan: {
    color: '#000',
    fontSize: 12,
    fontWeight: '500',
  },
  txtIsian: {
    color: '#000',
    fontSize: 12,
    fontWeight: '300',
    paddingHorizontal: 40,
    textAlign: 'center',
    marginTop: 5,
  },
  btnPesan: {
    height: 40,
    width: 300,
    backgroundColor: '#009788',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    marginTop: 20,
  },
  txtPsn: {
    color: '#fff',
    fontSize: 14,
    fontWeight: 'bold',
  },
});

export default Pesanan;
