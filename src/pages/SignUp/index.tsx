import React, {useEffect, useState} from 'react';
import {
  Text,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
  Image,
  View,
  TextInput,
  Alert,
} from 'react-native';

import styles from './styles';
import { InputText, TextView } from '../../components';
import { SurplusType } from '../../utils/type';
import { useRecoilState } from 'recoil';
import { dataUser } from '../../recoils';
import { checkEmail } from '../../utils/general';

const Signup = ({navigation}) => {
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [user, setUser] = useRecoilState(dataUser);
  const [errorEmail, setErrorEmail] = useState<SurplusType.error>({
    valid: true,
    message: ''
  })
  const [errorPassword, setErrorPassword] = useState<SurplusType.error>({
    valid: true,
    message: ''
  })

  const [errorConfirmPassword, setErrorConfirmPassword] = useState<SurplusType.error>({
    valid: true,
    message: ''
  })
  const handleRegister = () => {
    console.log('email', email, password)
    if (!email) {
        setErrorEmail({
            valid: false,
            message: 'Email tidak boleh kosong!'
        })
    } else if (!checkEmail(email)) {
        setErrorEmail({
            valid: false,
            message: 'Email tidak valid!'
        })
    } else {
        setErrorEmail({
            valid: true,
            message: ''
        })
    }

    if (!password) {
        setErrorPassword({
            valid: false,
            message: 'Kata sandi tidak boleh kosong!'
        })
    }

    if (password && confirmPassword && password !== confirmPassword) {
        setErrorConfirmPassword({
            valid: false,
            message: 'Kata sandi tidak boleh kosong!'
        })
    } else {
      if (password.length > 8) {
        setErrorPassword({
            valid: false,
            message: 'Kata sandi harus minimal 8 karakter!'
        })
      } else {
        setErrorPassword({
            valid: true,
            message: ''
        })
      }
      setErrorConfirmPassword({
        valid: true,
        message: ''
    })
    }

    if (
      email &&
      /\S+@\S+\.\S+/.test(email) &&
      password &&
      password === confirmPassword
    ) {
      Alert.alert('Daftar Berhasil', 'Daftar Akun Berhasil, Silahkan Login!', [
        {
          text: 'OK',
          onPress: () => {
            setUser({
                email: email,
                password: password
            })
            navigation.navigate('Login')
          },
        },
      ]);
    }
  };

  useEffect(() => {
    setEmail('');
    setPassword('');
    setConfirmPassword('');
  },[])

  useEffect(() => {
    setErrorEmail({
        valid: true,
        message: ''
    })
    setErrorPassword({
        valid: true,
        message: ''
    })
    setErrorConfirmPassword({
        valid: true,
        message: ''
    })
  },[email, password, confirmPassword])

  return (
    <SafeAreaView style={styles.container}>
      <View>
        <Image
          source={require('../../assets/images/header-bg.png')}
          style={styles.bgimg}
        />
        <View style={styles.viewHeader}>
          <Text style={styles.txtHeaderDaftar}>Daftar</Text>
          <Text style={styles.txtIsian}>Lengkapi isian untuk mendaftar</Text>
        </View>
      </View>
      <View style={styles.viewBottom}>
        <View>
        <InputText 
            label={'Email'}
            placeholder={'Massukan email'}
            value={email}
            onChangeInput={setEmail}
            error={errorEmail}
            isShowEye={false}
          />
        </View>

        <View style={styles.passwordContainer}>
            <InputText 
                label={'Kata Sandi'}
                placeholder={'Masukkan kata sandi'}
                value={password}
                onChangeInput={setPassword}
                error={errorPassword}
                isShowEye
            />
        </View>

        <View style={styles.passwordContainer}>
            <InputText 
                label={'Ulangi Kata Sandi'}
                placeholder={'Ulangi kata sandi'}
                value={confirmPassword}
                onChangeInput={setConfirmPassword}
                error={errorConfirmPassword}
                isShowEye
            />
        </View>

        <TouchableOpacity
          style={styles.btnDaftar}
          onPress={handleRegister}>
          <TextView
            style={styles.textButton}
            value={'Daftar'}
            />
        </TouchableOpacity>
        <TextView style={styles.txtPrivacy} value={'Dengan daftar atau masuk, Anda menerima syarat dan ketentuan serta kebijakan privasi'}/>
        <View style={styles.viewLogin}>
            <TextView style={styles.txtAkun} value={'Sudah Punya akun ?'}/>
          <TouchableOpacity onPress={() => navigation.navigate('Login')}>
            <TextView style={styles.txtMasuk} value={'Yuk masuk'}/>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Signup;