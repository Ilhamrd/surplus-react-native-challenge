import { StyleSheet } from "react-native";
import colours from "../../utils/colours";

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colours.white,
    },
    bgImage: {
      width: '100%',
      height: 450,
    },
    buttonNext: {
      position: 'absolute',
      right: 30,
      top: 30,
      height: 30,
      width: 60,
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth: 1,
      borderRadius: 20,
      borderColor: colours.text,
    },
    textNext: {
      color: colours.text,
      fontWeight: '500',
      fontSize: 12,
    },
    viewBottom: {
      backgroundColor: colours.white,
      borderTopRightRadius: 40,
      borderTopLeftRadius: 40,
      position: 'absolute',
      height: 450,
      width: '100%',
      alignItems: 'center',
      bottom: 0
    },
    textWelcome: {
      fontWeight: 'bold',
      fontSize: 16,
      marginTop: 40,
      color: colours.textBold,
    },
    textDesc: {
      textAlign: 'center',
      fontSize: 12,
      marginTop: 10,
      paddingHorizontal: 40,
    },
    btnDaftar: {
      backgroundColor: colours.primary,
      height: 40,
      width: '80%',
      borderRadius: 20,
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 30,
    },
    textDaftar: {
      textAlign: 'center',
      fontSize: 12,
      color: colours.white,
      fontWeight: '500',
    },
    btnMasuk: {
      borderWidth: 1,
      borderColor: colours.primary,
      height: 40,
      width: '80%',
      borderRadius: 20,
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 10,
    },
    textMasuk: {
      textAlign: 'center',
      fontSize: 12,
      color: colours.primary,
      fontWeight: '500',
    },
    textPrivacy: {
      textAlign: 'center',
      fontSize: 10,
      marginTop: 30,
      paddingHorizontal: 40,
    },
});

export default styles;