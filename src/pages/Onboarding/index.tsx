import React from 'react';
import {
  Text,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
  Image,
  View,
} from 'react-native';
import styles from './styles';
import { TextView } from '../../components';

const Onboarding = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <View>
        <Image
          source={require('../../assets/images/bg-onboarding.png')}
          style={styles.bgImage}
        />
        <TouchableOpacity
          style={styles.buttonNext}
          onPress={() => navigation.navigate('MainTabNav')}>
          <TextView style={styles.textNext} value={'Lewati'}/>
        </TouchableOpacity>
      </View>
      <View style={styles.viewBottom}>
        <TextView style={styles.textWelcome} value={'Selamat datang di Surplus'}/>
        <TextView style={styles.textDesc} value={'Selamatkan makanan berlebihan di aplikasi Surplus agar tidak terbuang sia-sia'}/>
        <TouchableOpacity
          style={styles.btnDaftar}
          onPress={() => navigation.navigate('Signup')}>
          <TextView style={styles.textDaftar} value={'Daftar'}/>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.btnMasuk}
          onPress={() => navigation.navigate('Login')}>
          <TextView style={styles.textMasuk} value={'Sudah punya akun? Masuk'}/>
        </TouchableOpacity>
        <TextView style={styles.textPrivacy} value={'Dengan daftar atau masuk, Anda menerima syarat dan ketentuan serta kebijakan privasi'}/>
      </View>
    </SafeAreaView>
  );
};

export default Onboarding;