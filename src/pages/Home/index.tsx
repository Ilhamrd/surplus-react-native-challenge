import React, {useState, useEffect} from 'react';
import {ImageSlider} from 'react-native-image-slider-banner';
import {
  Text,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
  Image,
  View,
  TextInput,
  Alert,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import styles from './styles';
import { useRecoilState } from 'recoil';
import { dataUser } from '../../recoils';
import { getListBaner } from '../../services';
const Home = ({navigation}) => {
  const [user, setUser] = useRecoilState(dataUser);
  const [dataBanner, setDataBanner] = useState(null)

  const handleLogout = async () => {
    try {
      await AsyncStorage.removeItem('name');
      Alert.alert('Logout Berhasil', 'Berhasil Keluar!', [
        {
          text: 'OK',
          onPress: () => {
            navigation.navigate('Login');
            setUser({
                email: '',
                password: ''
            })
          },
        },
      ]);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getListBaner(10).then((resp) => {
        setDataBanner(resp.data.results)
        console.log("rest", resp.data.results)
    })
  }, [])

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.viewHeader}>
        <View style={styles.viewLoc}>
          <Text style={styles.txtLoc}>Lokasi kamu</Text>
          <TouchableOpacity style={styles.iconCart}>
            <Image
              source={require('../../assets/images/cart.png')}
              style={{
                height: 25,
                width: 25,
                tintColor: '#fff',
              }}
            />
          </TouchableOpacity>
        </View>
        <Text style={styles.txtLoc1}>Indonesia</Text>
        <View style={styles.viewName}>
          <Text style={styles.txtWel}>Hi, {user.email} !</Text>
          {user.email && (
            <TouchableOpacity style={styles.iconLogout} onPress={handleLogout}>
              <Image
                source={require('../../assets/images/logout.png')}
                style={{
                  height: 20,
                  width: 20,
                  tintColor: '#fff',
                }}
              />
            </TouchableOpacity>
          )}
        </View>

        <TextInput
          style={styles.search}
          placeholder="Mau selamatkan makanan apa hari ini?"
          placeholderTextColor="#b2b2b2"
        />
        <TouchableOpacity style={styles.iconSearch}>
          <Image
            source={require('../../assets/images/search.png')}
            style={{
              height: 20,
              width: 20,
              tintColor: '#bebebe',
            }}
          />
        </TouchableOpacity>
      </View>
      <View style={styles.imgSlider}>
        <ImageSlider
          data={[
            {
              img: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/1.png',
            },
            {
              img: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/2.png',
            },
            {
              img: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/2.png',
            },
          ]}
          autoPlay={true}
          closeIconColor="#fff"
        />
      </View>
      <View style={styles.viewVoucher}>
        <TouchableOpacity>
          <Text style={styles.txtVoucher}>
            Mau lihat voucher kamu? Yuk daftar!
          </Text>
        </TouchableOpacity>
      </View>
      <View>
        <Image
          source={require('../../assets/images/bg-discover.png')}
          style={styles.bgimg}
        />
      </View>
      <View style={styles.viewKota}>
        <Text style={styles.txtKota}>
          Mau menyelamatkan banyak makanan di kota kamu?
        </Text>
        <Text style={styles.txtIsian}>
          Ayo segera daftarkan kotamu di aplikasi Surplus agar kita bisa
          beroperasi langsung dan kamu bisa menjadi bagian dari Surplus Hero!
        </Text>
      </View>
    </SafeAreaView>
  );
};

export default Home;