import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
    },
    viewHeader: {
      backgroundColor: '#009788',
      height: 150,
      width: '100%',
      borderBottomLeftRadius: 20,
      borderBottomRightRadius: 20,
    },
    viewLoc: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginHorizontal: 30,
      marginTop: 20,
    },
    txtLoc: {
      fontWeight: '300',
      color: '#fff',
      fontSize: 12,
    },
    iconCart: {
      width: 20,
      height: 20,
    },
    iconLogout: {
      marginTop: 30,
      marginLeft: 20,
    },
    iconSearch: {
      width: 20,
      height: 20,
      marginLeft: 340,
      marginTop: 25,
    },
    imgSlider: {
      height: 250,
      width: '85%',
      justifyContent: 'center',
      alignItems: 'center',
      alignSelf: 'center',
    },
    viewVoucher: {
      backgroundColor: '#f4f4f4',
      width: '85%',
      height: 50,
      justifyContent: 'center',
      alignItems: 'center',
      alignSelf: 'center',
      marginTop: 10,
      borderRadius: 8,
    },
    txtVoucher: {
      fontWeight: '500',
      color: '#53c0a9',
      fontSize: 13,
    },
    txtLoc1: {
      fontWeight: '500',
      color: '#fff',
      fontSize: 13,
      marginLeft: 30,
    },
    viewName: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    txtWel: {
      fontWeight: '500',
      color: '#fff',
      fontSize: 15,
      marginLeft: 30,
      marginTop: 30,
    },
    search: {
      height: 50,
      width: '85%',
      backgroundColor: '#fff',
      justifyContent: 'center',
      alignSelf: 'center',
      position: 'absolute',
      top: 123,
      borderWidth: 1,
      borderRadius: 10,
      borderColor: '#e6e6e6',
      paddingLeft: 10,
      fontSize: 12,
    },
    bgimg: {
      width: 200,
      height: 200,
      justifyContent: 'center',
      alignSelf: 'center',
      marginTop: 30,
    },
    viewKota: {
      justifyContent: 'center',
      alignSelf: 'center',
      alignItems: 'center',
      marginTop: 10,
    },
    txtKota: {
      color: '#000',
      fontSize: 12,
      fontWeight: '500',
    },
    txtIsian: {
      color: '#000',
      fontSize: 12,
      fontWeight: '300',
      paddingHorizontal: 40,
      textAlign: 'center',
    },
});

export default styles;