/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {
  Text,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
  Image,
  View,
  TextInput,
  Alert,
} from 'react-native';

const Forgot = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [isFormValid, setIsFormValid] = useState(false);
  const [emailTouched, setEmailTouched] = useState(false);
  const [checkEmail, setCheckEmail] = useState('');
  const [borderEmail, setBorderEmail] = useState('#eeeeee');

  const handleForgot = () => {
    if (!email) {
      setCheckEmail('Email tidak boleh kosong!');
    } else if (!/\S+@\S+\.\S+/.test(email)) {
      setCheckEmail('Email tidak valid!');
    }

    if (email && /\S+@\S+\.\S+/.test(email)) {
      Alert.alert('Gagal', 'Email tidak terdaftar!', [
        {
          text: 'Kembali ke Login',
          onPress: () => navigation.navigate('Login'),
        },
      ]);
    }
  };

  const handleFormChange = () => {
    if (email) {
      setIsFormValid(true);
    } else {
      setIsFormValid(false);
    }
  };

  const handleEmailChange = text => {
    setEmail(text);
    setEmailTouched(true);
    if (!text) {
      setCheckEmail('');
      setBorderEmail('#eeeeee');
    } else {
      setBorderEmail(text.length >= 1 ? '#009788' : '#eeeeee');
    }
    handleFormChange();
  };

  return (
    <SafeAreaView style={styles.container}>
      <View>
        <Image
          source={require('../assets/images/bg-forgot.png')}
          style={styles.bgimg}
        />
      </View>
      <View style={styles.viewHeader}>
        <Text style={styles.txtHeaderForgot}>Lupa kata sandi?</Text>
        <Text style={styles.txtIsian}>
          Masukkan alamat email agar kita bantu kamu dalam
        </Text>
        <Text style={styles.txtIsian}>memulihkan akun kamu</Text>
      </View>
      <View style={styles.viewBottom}>
        <View>
          <Text style={styles.txtEmail}>E-mail</Text>
          <TextInput
            style={[
              styles.textInput,
              {borderColor: borderEmail},
              emailTouched && checkEmail ? styles.errorInput : null,
            ]}
            placeholder="Alamat email kamu"
            placeholderTextColor="#b2b2b2"
            value={email}
            onChangeText={handleEmailChange}
          />
          {emailTouched && checkEmail ? (
            <Text style={styles.errorText}>{checkEmail}</Text>
          ) : null}
        </View>
        <TouchableOpacity
          style={[
            styles.btnDaftar,
            {
              backgroundColor: isFormValid ? '#419489' : '#eeeeee',
            },
          ]}
          onPress={handleForgot}
          disabled={!isFormValid}>
          <Text
            style={{
              color: isFormValid ? '#fff' : '#c0c0c0',
              fontWeight: 'bold',
              fontSize: 15,
            }}>
            Kirim
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  bgimg: {
    width: 300,
    height: 300,
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 70,
  },
  viewHeader: {
    marginTop: 30,
    marginLeft: 30,
  },
  txtHeaderForgot: {
    color: '#000',
    fontSize: 20,
    fontWeight: 'bold',
  },
  txtIsian: {
    color: '#000',
    fontSize: 12,
    fontWeight: '300',
  },
  viewBottom: {
    borderTopRightRadius: 40,
    borderTopLeftRadius: 40,
    marginTop: 10,
  },
  txtEmail: {
    color: '#000',
    marginLeft: 30,
    fontWeight: '500',
    marginTop: 20,
  },
  btnDaftar: {
    backgroundColor: '#eeeeee',
    height: 50,
    width: '85%',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 20,
  },
  txtForgot: {
    textAlign: 'right',
    fontSize: 10,
    marginTop: 10,
    marginRight: 35,
    textDecorationLine: 'underline',
  },
  textInput: {
    borderWidth: 1,
    borderColor: '#eeeeee',
    height: 50,
    width: '85%',
    borderRadius: 10,
    marginTop: 5,
    paddingLeft: 20,
    paddingRight: 10,
    flexDirection: 'row',
    alignSelf: 'center',
  },
  errorInput: {
    borderColor: 'red',
  },
  errorText: {
    color: 'red',
    fontSize: 12,
    marginTop: 5,
    marginLeft: 30,
    marginRight: 30,
  },
});

export default Forgot;
