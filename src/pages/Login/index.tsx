import React, {useState, useEffect} from 'react';
import {
  Text,
  TouchableOpacity,
  Image,
  View,
  Alert,
} from 'react-native';
import { useRecoilState } from 'recoil';
import { InputText } from '../../components';
import styles from './styles';
import { dataUser } from '../../recoils';
import { SurplusType } from '../../utils/type';
import { checkEmail } from '../../utils/general';

const Login = ({navigation}) => {
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [isFormValid, setIsFormValid] = useState<boolean>(false);

  const [user, setUser] = useRecoilState(dataUser);
  const [errorEmail, setErrorEmail] = useState<SurplusType.error>({
    valid: true,
    message: ''
  })
  const [errorPassword, setErrorPassword] = useState<SurplusType.error>({
    valid: true,
    message: ''
  })

  const handleLogin = () => {
    console.log("------_______------", user)
    if(!checkEmail(email))
      setErrorEmail({
        valid: false,
        message: 'Email tidak valid.'
      })
    else 
      setErrorEmail({
        valid: true,
        message: ''
      })
    
    if(password.length < 8)
      setErrorPassword({
        valid: false,
        message: 'Password minimal 8 char'
      })
    else 
      setErrorPassword({
        valid: true,
        message: ''
      })
    if(user) {
      if (email === user.email && password === user.password) {
        Alert.alert('Login Berhasil', 'Berhasil Masuk!', [
          {
            text: 'OK',
            onPress: () => {
              navigation.navigate('MainTabNav');
              setEmail('');
              setPassword('');
            },
          },
        ]);
      } else {
        Alert.alert('Login Gagal', 'Email atau Password Salah!');
      }
    }
    
  };

  useEffect(() => {
    if(email && password) {
      setIsFormValid(true);
    }
  }, [email, password])


  return (
    <View style={styles.container}>
      <View>
        <Image
          source={require('../../assets/images/header-bg.png')}
          style={styles.bgimg}
        />
      </View>
      <View style={styles.viewBottom}>
        <View>
          <InputText 
            label={'Email'}
            placeholder={'Alamat email kamu'}
            value={email}
            onChangeInput={setEmail}
            error={errorEmail}
            isShowEye={false}
          />
        </View>

        <View style={styles.passwordContainer}>
          <InputText 
            label={'Kata Sandi'}
            placeholder={'Masukkan kata sandi'}
            value={password}
            onChangeInput={setPassword}
            error={errorPassword}
            isShowEye
          />
        </View>

        <TouchableOpacity
          onPress={() => navigation.navigate('Forgot')}
          style={styles.btnLupa}>
          <Text style={styles.txtForgot}>Lupa kata sandi?</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.btnDaftar,
            {
              backgroundColor: isFormValid ? '#419489' : '#eeeeee',
            },
          ]}
          onPress={handleLogin}
          disabled={!isFormValid}>
          <Text
            style={{
              color: isFormValid ? '#fff' : '#c0c0c0',
              fontWeight: 'bold',
              fontSize: 15,
            }}>
            Masuk
          </Text>
        </TouchableOpacity>
        <View style={styles.viewLogin}>
          <Text style={styles.txtAkun}>Belum punya akun ?</Text>
          <TouchableOpacity onPress={() => navigation.navigate('Signup')}>
            <Text style={styles.txtMasuk}>Yuk daftar</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Login