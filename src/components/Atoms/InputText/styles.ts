import { StyleSheet } from 'react-native';
import colours from '../../../utils/colours';

const styles: any = StyleSheet.create({

    errorText: {
        color: colours.dangerBgRed,
        fontSize: 12,
        marginTop: 5,
        marginLeft: 30,
        marginRight: 30,
    },
    textInputError: {
        borderColor: colours.dangerBgRed
    },
    textInput: {
        borderWidth: 1,
        borderColor: '#eeeeee',
        height: 50,
        width: '85%',
        borderRadius: 10,
        marginTop: 8,
        paddingLeft: 20,
        paddingRight: 10,
        flexDirection: 'row',
        alignSelf: 'center',
    },
    iconSandi: {
        position: 'absolute',
        right: 50,
        top: 60,
    },
    imageEye: {
        height: 20,
        width: 20,
        tintColor: '#b2b2b2',
    },
    label: {
        color: colours.black,
        marginLeft: 30,
        fontWeight: '500',
        marginTop: 16,
    }
})

export default styles