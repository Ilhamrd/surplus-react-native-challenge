import React, { useState } from "react";
import { Image, TextInput, TouchableOpacity } from "react-native";
import { SurplusType } from "../../../utils/type";
import TextView from "../TextView";
import styles from "./styles";
interface Props {
    style?: any,
    placeholder: string,
    label: string,
    value: string,
    onChangeInput: (value: string) => void;
    error: SurplusType.error,
    isShowEye: boolean
}
const InputText = ({
    style = '',
    placeholder = '',
    label = '',
    value = '',
    onChangeInput,
    error,
    isShowEye = false
}: Props) => {

    const [showPassword, setShowPassword] = useState<boolean>(isShowEye);

    return (
        <>
            <TextView
                value={label}
                style={styles.label}
            />
            <TextInput 
                style={[style, styles.textInput, !error.valid ? styles.textInputError : null]}
                placeholder={placeholder}
                placeholderTextColor={'#b2b2b2'}
                value={value}
                onChangeText={(text: string) => onChangeInput(text)}
                secureTextEntry={showPassword}
            />
            {!error.valid && (
                <TextView 
                    style={styles.errorText}
                    value={error.message}
                />
            )}
            {isShowEye && (
                <TouchableOpacity
                    onPress={() => setShowPassword(!showPassword)}
                    style={styles.iconSandi}>
                    <Image
                        source={
                            showPassword
                            ? require('../../../assets/images/eye-show.png')
                            : require('../../../assets/images/eye-hide.png')
                        }
                        style={styles.imageEye}
                    />
                </TouchableOpacity>
            )}
           

        </>
        
    )
}

export default React.memo(InputText)