import { StyleSheet } from 'react-native';
import colours from '../../../utils/colours';

const styled: any = StyleSheet.create({
    danger: {
        color: colours.dangerBgRed
    },
    warning: {
        color: colours.warning
    },
    default: {
        color: colours.black
    }
})

export default styled;