import React from 'react'
import { Text } from 'react-native'
import styles from './styles'

interface Props  {
    value: string;
    mode?: 'danger'| 'warning' | 'default'
    style?: any
}

const TextView = ({
    value = '',
    style = '',
    mode = 'default'
}: Props) => {

    const styled = mode === 'danger' ? styles.danger : mode === 'warning' ? styles.warning : style.default ;
    return (
        <Text style={[style, styled]}>
            {value}
        </Text>
    )
}

export default React.memo(TextView)