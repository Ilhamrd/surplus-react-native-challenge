import React, { useState, useEffect } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Image} from 'react-native';
import { createNativeStackNavigator} from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Onboarding from '../pages/Onboarding';
import Login from '../pages/Login';
import Signup from '../pages/SignUp';
import Forgot from '../pages/Forgot';
import Discover from '../pages/Home';
import Pesanan from '../pages/Pesanan';

const AuthStack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const MainTabNav = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarActiveTintColor: '#009788',
      }}>
      <Tab.Screen
        name="HOME"
        component={Discover}
        options={{
          tabBarIcon: ({focused}) => {
            return (
              <Image
                source={require('../assets/images/discover.png')}
                style={{
                  height: 20,
                  width: 20,
                  tintColor: focused ? '#009788' : '#b2b2b2',
                }}
              />
            );
          },
        }}
      />
      <Tab.Screen
        name="PESANAN"
        component={Pesanan}
        options={{
          tabBarIcon: ({focused}) => {
            return (
              <Image
                source={require('../assets/images/pesanan.png')}
                style={{
                  height: 20,
                  width: 20,
                  tintColor: focused ? '#009788' : '#b2b2b2',
                }}
              />
            );
          },
        }}
      />
    </Tab.Navigator>
  );
};

const AuthStackNav = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    const checkLoggedIn = async () => {
      try {
        const value = await AsyncStorage.getItem('name');
        if (value !== null) {
          setIsLoggedIn(true);
        }
      } catch (e) {
        console.log('Error data!!', e);
      }
    };

    checkLoggedIn();
  }, []);

  return (
    <AuthStack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      {!isLoggedIn && (
        <>
          <AuthStack.Screen name="Onboarding" component={Onboarding} />
          <AuthStack.Screen name="Login" component={Login} />
          <AuthStack.Screen name="Signup" component={Signup} />
          <AuthStack.Screen name="Forgot" component={Forgot} />
          
        </>
      )}
      <AuthStack.Screen name="MainTabNav" component={MainTabNav} />
    </AuthStack.Navigator>
  );
};

export default AuthStackNav;
